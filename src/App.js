import React, {Component, Fragment} from 'react';
import Blog from "./containers/Blog/Blog";

class App extends Component {
  state = {
    blogShown: true
  };

  toggleBlog = () => {
    this.setState(prevState => {
      return {blogShown: !prevState.blogShown};
    });
  };

  render() {
    let content = null;
    if (this.state.blogShown) {
      content = <Blog stuff="hey" />;
    }

    return (
      <Fragment>
        <button onClick={this.toggleBlog}>Toggle Blog</button>
        {content}
      </Fragment>
    )
  }
}

export default App;
