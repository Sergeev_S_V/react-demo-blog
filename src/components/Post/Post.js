import React, {PureComponent} from 'react';
import './Post.css';

class Post extends PureComponent {
  render() {
    return (
      <article className="Post">
        <h1>{this.props.title}</h1>
        <div className="Info">
          <div className="Author">{this.props.author}</div>
        </div>
      </article>
    );
  }
}

export default Post;